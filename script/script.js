var get_main_menu_header = document.querySelector('.main-menu-header');
var get_preference = document.querySelector('.preferences-outer');
var get_searches_filter_wrapper = document.querySelector('.searches-filter-wrapper');
var get_food_menu_main = document.querySelector('.food-menu-main');
var get_cart = document.querySelector('.cart-main');



var get_header = document.querySelector('.main-header');
var window_width = window.matchMedia("(max-width: 992px)");


// header height fixation=============
if (get_header) {
    function common_header_fixation() {
        var header_height = get_header.offsetHeight;
        var header_next = get_header.nextElementSibling;
        header_next.style.marginTop = +header_height + "px";
    };
    common_header_fixation();
}

// main header height fixation=============
if (get_main_menu_header) {
    function menu_header_fixation(window_width) {
        if (window_width.matches) {
            var main_menu_header_height = get_main_menu_header.offsetHeight;
            var preference_height = get_preference.offsetHeight;
            var get_searches_filter_wrapper_height = get_searches_filter_wrapper.offsetHeight;
            var get_cart_height = get_cart.offsetHeight;
            get_preference.style.top = +main_menu_header_height + "px";
            get_searches_filter_wrapper.style.top = +main_menu_header_height + preference_height + "px";
            get_food_menu_main.style.marginTop = +main_menu_header_height + preference_height + get_searches_filter_wrapper_height + "px";
            get_cart.style.top = +main_menu_header_height + "px";
            get_cart.style.maxHeight = "calc(100vh - " + main_menu_header_height + "px"
            ")";
        }
    }
    menu_header_fixation(window_width);
    window_width.addListener(menu_header_fixation);
}