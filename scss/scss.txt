
 //  customize popup
 .popup .popup-inner {
     .customize-options {
         padding: 30px 20%;
         .customize-option {
             margin-bottom: 15px;
             cursor: pointer;
             .custom-checkbox {
                 position: relative;
                 @include flex;
                 @include align-items(center);
                 .checkmark {
                     position: absolute;
                     height: 20px;
                     width: 20px;
                     border: 1px solid $color-primary;
                     border-radius: 2px;
                     &::before {
                         content: "";
                         display: none;
                         position: absolute;
                         height: 5px;
                         width: 10px;
                         border-left: 2px solid $color-white;
                         border-bottom: 2px solid $color-white;
                         left: 3px;
                         top: 4px;
                         transform: rotate(-45deg);
                     }
                 }
                 input {
                     position: absolute;
                     height: 0;
                     opacity: 0;
                     &:checked~.checkmark {
                         background: $color-primary;
                         &::before {
                             display: inline-block;
                         }
                     }
                 }
             }
             .custom-checkbox .text {
                 margin-left: 30px
             }
         }
     }
 }
 
 @media(max-width:767px) {
     .popup .popup-inner .customize-options {
         padding: 30px 10%;
     }
 }